# RocketWorks-HPR™

RocketWorks-HPR™ is an OpenRocket driven design and optimization add-in for use with SolidWorks.

RocketWorks™ will be RocketWorks-HPR™ on steroids for collegiate sounding rocket design and optimization using MATLAB and SolidWorks.